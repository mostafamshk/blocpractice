import 'dart:math';

import 'package:blocpractice/businessLogic/cubits/inputText_cubit.dart';
import 'package:blocpractice/businessLogic/cubits/inputText_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SecondPage extends StatefulWidget {
  const SecondPage({super.key});

  @override
  State<SecondPage> createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: Column(
          children: [
            BlocConsumer<TextCubit, TextState>(
              listener: (listenerContext, state) {
                // TODO: implement listener
              },
              builder: (builderContext, state) {
                return Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 100, horizontal: 20),
                    child: Text("Your Random Number : ${state.text}"));
              },
            ),
            MaterialButton(
              onPressed: () {
                BlocProvider.of<TextCubit>(context)
                    .changeText(Random().nextInt(100).toString());
              },
              child: Icon(Icons.create_rounded),
            )
          ],
        ),
      ),
    );
  }
}
