import 'package:blocpractice/presentation/pages/home.dart';
import 'package:blocpractice/presentation/pages/second.dart';
import 'package:flutter/material.dart';

class AppRouter {
  MaterialPageRoute? onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case "/":
        return MaterialPageRoute(builder: (context) {
          return HomePage();
        });
      case "/second":
        return MaterialPageRoute(
          builder: (context) {
            return SecondPage();
          },
        );
      default:
        return null;
    }
  }

}
