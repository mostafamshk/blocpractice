import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class HamburgerMenu extends StatefulWidget {
  const HamburgerMenu({super.key});

  @override
  State<HamburgerMenu> createState() => _HamburgerMenuState();
}

class _HamburgerMenuState extends State<HamburgerMenu> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            buildHeader(context),
            buildMenuItems(context),
          ],
        ),
      ),
    );
  }

  Widget buildHeader(BuildContext context) {
    return Container();
  }

  Widget buildMenuItems(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24),
      child: Wrap(
        runSpacing: 16,
        children: [
          ListTile(
            leading: Icon(Icons.home_filled),
            onTap: () {
              Navigator.of(context).pushNamed("/");
            },
            title: Text("Home"),
          ),
          ListTile(
            leading: Icon(Icons.home_filled),
            onTap: () {
              Navigator.of(context).pushNamed("/");
            },
            title: Text("Home"),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.home_filled),
            onTap: () {
              Navigator.of(context).pushNamed("/");
            },
            title: Text("Home"),
          ),
        ],
      ),
    );
  }
}
