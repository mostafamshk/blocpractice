// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import "counter_cubit.dart";

class CounterState {
  int state;
  bool wasIncremented;
  CounterState({
    required this.state,
    required this.wasIncremented,
  });

  


  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'state': state,
      'wasIncremented': wasIncremented,
    };
  }

  factory CounterState.fromMap(Map<String, dynamic> map) {
    return CounterState(
      state: map['state'] as int,
      wasIncremented: map['wasIncremented'] as bool,
    );
  }

  String toJson() => json.encode(toMap());

  factory CounterState.fromJson(String source) => CounterState.fromMap(json.decode(source) as Map<String, dynamic>);
}
