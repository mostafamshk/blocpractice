import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import "package:meta/meta.dart";

import 'counter_state.dart';

class CounterCubit extends Cubit<CounterState> with HydratedMixin {
  CounterCubit() : super(CounterState(state: 0, wasIncremented: false));

  void increment() {
    emit(CounterState(state: state.state + 1, wasIncremented: true));
  }

  void decrement() {
    emit(CounterState(state: state.state - 1, wasIncremented: false));
  }

  @override
  CounterState? fromJson(Map<String, dynamic> json) {
    return CounterState.fromMap(json);
  }

  @override
  Map<String, dynamic>? toJson(CounterState state) {
    return state.toMap();
  }
}
