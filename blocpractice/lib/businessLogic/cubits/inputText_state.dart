// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'inputText_cubit.dart';

class TextState {
  String text;

  TextState({
    required this.text,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'text': text,
    };
  }

  factory TextState.fromMap(Map<String, dynamic> map) {
    return TextState(
      text: map['text'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory TextState.fromJson(String source) =>
      TextState.fromMap(json.decode(source) as Map<String, dynamic>);
}
