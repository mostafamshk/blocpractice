// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';

import 'package:hydrated_bloc/hydrated_bloc.dart';

import 'package:blocpractice/businessLogic/cubits/counter_cubit.dart';
import 'package:blocpractice/businessLogic/cubits/inputText_state.dart';

import 'inputText_state.dart';

class TextCubit extends Cubit<TextState> with HydratedMixin {
  CounterCubit counterCubit;
  late StreamSubscription counterStreamSubscription;

  TextCubit(
    this.counterCubit,
  ) : super(TextState(text: "0")) {
    counterStreamSubscription = counterCubit.stream.listen((event) {
      if (event.state % 3 == 0 && event.state != 0) {
        changeText("9999999");
      }
    });
  }

  void changeText(String text) {
    emit(TextState(text: text));
  }

  @override
  TextState? fromJson(Map<String, dynamic> json) {
    // TODO: implement fromJson
    return TextState.fromMap(json);
  }

  @override
  Map<String, dynamic>? toJson(TextState state) {
    // TODO: implement toJson
    return state.toMap();
  }
}
